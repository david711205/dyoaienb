# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/aa/aa/targets/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/connectionManager/ConnectionCOM.cpp" "/home/aa/aa/cmake_targets/lte_build_oai/build/lmsSDR/connectionManager/CMakeFiles/ConnectionManager.dir/ConnectionCOM.cpp.o"
  "/home/aa/aa/targets/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/connectionManager/ConnectionManager.cpp" "/home/aa/aa/cmake_targets/lte_build_oai/build/lmsSDR/connectionManager/CMakeFiles/ConnectionManager.dir/ConnectionManager.cpp.o"
  "/home/aa/aa/targets/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/connectionManager/ConnectionUSB.cpp" "/home/aa/aa/cmake_targets/lte_build_oai/build/lmsSDR/connectionManager/CMakeFiles/ConnectionManager.dir/ConnectionUSB.cpp.o"
  "/home/aa/aa/targets/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/connectionManager/lmsComms.cpp" "/home/aa/aa/cmake_targets/lte_build_oai/build/lmsSDR/connectionManager/CMakeFiles/ConnectionManager.dir/lmsComms.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ENABLE_USB_CONNECTION"
  "_CRT_SECURE_NO_WARNINGS"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/aa/aa/targets/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/LMS_StreamBoard"
  "/home/aa/aa/targets/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/connectionManager"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
